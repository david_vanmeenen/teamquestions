from rest_framework import serializers

from questions.models import Question
from teamquestions.serializers import UserSummarySerializer


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    def to_representation(self, instance):
        self.fields['author'] = UserSummarySerializer(read_only=True)

        return super(QuestionSerializer, self).to_representation(instance)

    class Meta:
        model = Question
        fields = ["id", "title", "text", "author", "timestamp"]
        # TODO: is_answered and has_accepted_answer must be included
